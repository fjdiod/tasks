#O(n)
#Writing list imto array --- n
#Creating new list --- another n/2

class Node():
    
    def __init__(self, data=None, next_node=None):
        self.data = data
        self.next_node = next_node
        
    def __str__(self):
        return str(self.get_data())

    def next(self):
        if not self.next_node:
            raise StopIteration()
        return self.get_next()
    
    def get_data(self):
        return self.data
        
    def get_next(self):
        return self.next_node
        
    def set_next(self, new_next):
        self.next_node = new_next
        

class LinkedList():

    def __init__(self, head=None):
        self.head = head
                
    def __iter__(self):
        return self
    
    def next(self):
        if not self.head:
            raise StopIteration()
        return self.head.get_next()
        
    def insert(self, data):
        new_node = Node(data)
        new_node.set_next(self.head)
        self.head = new_node
    
    def size(self):
        pos = self.head
        l = 0
        while pos:
            l += 1
            pos = pos.get_next()
        return l
        
        
def repack(linked_list):
    pos = linked_list.head
    a = []
    while pos:
        a += [pos.get_data()]
        pos = pos.get_next()
        
    a = a[::-1]
    repacked_list = LinkedList()
    for i in xrange(len(a)/2):
        repacked_list.insert(a[i])
        repacked_list.insert(a[len(a)-i-1])
    if len(a)%2 == 1:
        repacked_list.insert(a[len(a)/2])
    return repacked_list
        
ls = LinkedList()

ls.insert(1)
ls.insert(2)
ls.insert(3)
ls.insert(4)
ls.insert(5)

repacked = repack(ls)

pos = repacked.head

while pos:
    print pos.get_data()
    pos = pos.get_next()