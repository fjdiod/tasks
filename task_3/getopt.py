def print_help(args_struct):
    tmp1 = ''
    tmp2 = ''
    for i in args_struct:
        if args_struct[i][1]:
            tmp1 += '--' + i + ' value '
        elif args_struct[i][0]:
            tmp2 += '[--' + i + ' value] '
        else:
            tmp2 += '[--' + i + '] '
    print tmp1 + tmp2[:-1]


def getopt(args, args_struct):
    args = args.split()
    ans = {i:0 for i in args_struct}
    number_req = sum([args_struct[i][1] for i in args_struct])
    count = 0
    i = 0
    while i < len(args):
        if args[i] == '--help':
            print_help(args_struct)
            return None
        if args[i][:2] != '--':
            print "The" + args[i] + " argument not starting from '--'."
            return None
        else:
            tmp = args[i][2:]
        if tmp.find('=') != -1:
            tmp = tmp.split('=')
            if tmp[0] in args_struct:
                
                if args_struct[tmp[0]][1]:
                    count += 1
                if args_struct[tmp[0]][0]:
                    if tmp[1] == '':
                        print 'Value for the argument was not presented.'
                        return None
                    ans[tmp[0]] = tmp[1]
                else:
                    print 'Value asignment to the boolean argument.'
                    return None
            else:
                print 'The argument is not in structure.'
                return None
            i += 1
        elif tmp in args_struct:
            
            if args_struct[tmp][1]:
                count += 1 
            if args_struct[tmp][0]:
                if args[i+1][:2] != '--':
                    ans[tmp] = args[i+1]
                    i += 2
                else:
                    print 'Value for the argument was not presented.'
                    return None
            else:
                ans[tmp] = 1
                i += 1
        else:
            print 'The argument is not in structure.'
            return None
    if number_req != count:
        print 'Not all required arguments were presented.'
        return None
    else:
        return ans
        
args_struct = {'out': (1,1), 'in':(1,0), 'help':(0,0), 'mode':(0,0), 'att':(0,0)}

getopt('--help', args_struct)

print(getopt('--out awawd --mode', args_struct))