#include <iostream>

using namespace std;

void split(int n, int m) {
    if(n%m == 0) {
        for(int i = 0; i < n; i += (int)n/m) {
            cout << i << " " << i +(int)n/m - 1 << endl;
        }
    } else {
        if(n%m%2 == 0) {
            for(int i = (int)n%m/2; i < n - (int)n%m/2; i += (int)n/m) {
                cout << i << " " << i +(int)n/m - 1 << endl;
            }
        } else {
            for(int i = (int)n%m/2; i < n - (int)n%m/2-1; i += (int)n/m) {
                cout << i << " " << i +(int)n/m - 1 << endl;
            }
        }
    }

}

int main() {
    split(10, 4);
    return 0;
}
