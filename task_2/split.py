def split_list(n, m):
    if n%m == 0:
        return [(i, i + n/m-1) for i in range(0, n, n/m)]
    elif n%m%2 == 0:
        return [(i, i + n/m-1) for i in range(n%m/2, n-n%m/2, n/m)]
    else:
        return [(i, i + n/m-1) for i in range(n%m/2, n-n%m/2-1, n/m)]
        
#test
print(split_list(10,4))