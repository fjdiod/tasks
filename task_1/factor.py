def factor(n):
    ans = [[1]]
    for i in xrange(2,n+1):
        tmp = []
        j = 2
        while j*j <= i:
            if i%j == 0:
                tmp += [j]
                i //= j
            else:
                j += 1
                
        if i > 1:
            tmp += [i]
        ans += [tmp]
    return ans
    
#test
print(factor(10))