#Я когда-то захотел поанализировать ценообразование на авито. Выбрал категорию мобильных телефонов, как самую многочисленную.
#В этом скрипте как раз скачивается вся доступная на авито информация по телефонам(в этой версии в питере). Также загружаются фотки, я думал использовать их,
#чтобы определять насколько телефон поломан(до этого правда не дошло).

import requests
from bs4 import BeautifulSoup
from time import time, sleep
import datetime as dt
import shutil
import re
import sys

path_to_out = sys.args[1]



def load_phone_data(url):
    N_requests = 1
    while N_requests <= 5:
        try:
            page = requests.get(url)
            break
        except Exception as e:
            print 'Conection lost!'
            sleep(N_requests*30)
            if N_requests == 5:
                return None
    soup = BeautifulSoup(page.text)
    
    price = soup.select('.p_i_price > span')
    name = soup.select('.h1')
    date = soup.select('.item-subtitle')
    if not date:
        return None, None
    date = proc_date(soup2text(date))   
    description = soup.select('#desc_text')
    
    photos = soup.select('.b-zoom-gallery > div.b-gallery > div.gallery-list-wrapper > ul.gallery-list > li > a')
    photos = map(lambda x: 'https:' + x['href'] , photos)
    data1 = [url, date]
    data2 =[price, name, description]
    data2 = map(soup2text, data2)
    return data1 + data2, photos

#data[0] == data1, data[1] == data2, data[2] == photos

    
def proc_date(date):
    
    calend = {u'января':1, u'февраля':2, u'марта':3, u'апреля':4, u'майя':5, u'июня':6, u'июля':7, 
         u'августа':8, u'сентября':9, u'октября':10, u'ноября':11, u'декабря':12}
    
    day = date.split('.')[0].split()[1]
    date_year = dt.datetime.now().year
    if day == u'сегодня':
        date_to_write = str(dt.datetime.now()).split()[0]
    else:
        if day == u'вчера':
            date_to_write = str(dt.datetime.now() - dt.timedelta(days=1)).split()[0]
        else:
            date_day = int(date.split('.')[0].split()[1])
            date_month = date.split('.')[0].split()[2]
            #print month
            date_month = calend[date_month]
            #print month
            date_to_write = str(dt.date(date_year,date_month,date_day))
    return date_to_write

def write_data(data, photos, file_to_write, stamp, base_path):
    data = [str(stamp)] + data
    data = map(lambda x: '"' + x.replace('"','""') + '"', data)
    tmp = data[0]
    data_str = data[1:]
    
    str_to_write = reduce(lambda res, x: res + ',' + x, data_str, tmp)
    str_to_write += '\n'
    str_to_write = str_to_write.encode('utf-8')
    file_to_write.write(str_to_write)
    file_to_write.flush()
    
    number_of_photo = 1
    if photos:
        for url in photos:
            try:
                response = requests.get(url, stream=True)
            except:
                sleep(30):
                continue
            file_name = base_path + str(stamp) + '_' + str(number_of_photo) + '.jpg'
            number_of_photo += 1
            with open(file_name, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
        
def soup2text(data):
    if data:
        data = data[0].getText()
        data =  ' '.join(data.split())
    else:
        data = str(None)
    return data
    
def get_last_page(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text)
    last = soup.select('a.pagination-page')
    number = re.findall('[0-9]+',last[len(last)-1]['href'])
    return number[0]
    
def get_pages_links(url):
    N_requests = 1
    while N_requests <= 5:
        try:
            page = requests.get(url)
            break
        except Exception as e:
            print 'Conection lost!'
            sleep(N_requests*30)
            if N_requests == 5:
                return None              
    soup = BeautifulSoup(page.text)
    links_befor = soup.select('div.js-catalog_before-ads > div > div.description > h3 > a')
    links_after = soup.select('div.js-catalog_after-ads > div > div.description > h3 > a')
    links = links_befor + links_after
    links = map(lambda x: x['href'], links)
    return links
	
    
first_Url = 'https://www.avito.ru/sankt-peterburg/telefony'
number_of_pages = int(get_last_page(first_Url)) - 10
f = open( pathToOut + '/phones.csv', 'w')
base_path = path_to_out
while number_of_pages >= 1:
    print number_of_pages
    page_Url = first_Url + '?p=' + str(number_of_pages)
    links = get_pages_links(page_Url)
    if not links:
        sleep(60)
        continue
    sleep(2)
    number_of_pages -= 1
    counter = 0.0
    for link in links:
        phone_Url = 'https://www.avito.ru' + link
        print counter/50.0 * 100
        counter += 1.0
        data, photos = load_phone_data(phone_Url)
        if data:
            write_data(data, photos, f, time(),base_path)
            sleep(5)
        else:
            sleep(60)
            continue
    
f.close()