#Это была учебная работа.
#Тут пытался применять сверточные сети для распознавания лиц(10 классов).

from PIL import Image
from time import time
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.datasets import fetch_lfw_people
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from keras.utils import np_utils
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.callbacks import History
import tensorflow as tf

tf.set_random_seed(42)
np.random.seed(1)

def reshapeDataset(dataset, newWidth, newHeight):
  new_dataset = []
  for data in dataset:
    size = (newWidth, newHeight)
    img = Image.fromarray(data)
    img = img.resize(size)
    img = np.array(img)
    new_dataset.append(img[np.newaxis, :, :])

  return np.array(new_dataset)


lfw_people = fetch_lfw_people(min_faces_per_person=70, resize=0.4)

n_samples, h, w = lfw_people.images.shape

#reshaping data
X = reshapeDataset( lfw_people.images, 32, 32)
h, w = 32, 32
X = np.reshape(X, (1288, 32, 32, 1))
print(X.shape)

# the label to predict is the id of the person
y = lfw_people.target
target_names = lfw_people.target_names
nb_class = len(target_names)
''' 
Classes:
    0- Ariel Sharon
    1- Colin Powell
    2- Donald Rumsfeld
    3- George W Bush
    4- Gerhard Schroeder
    5- Hugo Chavez
    6- Tony Blair
'''


print("Total dataset size:")
print("n_samples: %d" % n_samples)
print("n_classes: %d" % nb_class)


# split into a training and testing set
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=42)

Y_train = np_utils.to_categorical(y_train, nb_class)
Y_test = np_utils.to_categorical(y_test, nb_class)

model = Sequential()

#1 conv layer. 32 3x3 convolutions folowed by Relu nonlinearity
model.add(Convolution2D(32, 3, 3,
                border_mode='valid',
                input_shape=( 32, 32, 1)))
model.add(Activation('relu'))

#2 conv layer. 64 5x5 convolutions folowed by Relu nonlinearity
model.add(Convolution2D(64, 5, 5))
model.add(Activation('relu'))

#3 conv layer. 64 5x5 convolutions folowed by Relu nonlinearity
model.add(Convolution2D(64, 5, 5))
model.add(Activation('relu'))

#max pooling
model.add(MaxPooling2D(pool_size=(3, 3)))

#dropout for regularization
model.add(Dropout(0.25))

model.add(Flatten())

#1 dense layer followed by Relu
model.add(Dense(400))
model.add(Activation('relu'))

#dropout for regularization
model.add(Dropout(0.5))

#2 dense layer followed
model.add(Dense(nb_class))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adadelta',\
                     metrics=["accuracy"])


#40 epochs with 100 samples in batch
model.fit(X_train, Y_train, batch_size=100,
                               nb_epoch=40, verbose=2,
                               validation_data=(X_test, Y_test), shuffle=True)

print("Predicting people's names on the test set")
t0 = time()
y_pred = model.predict_classes(X_test)
print("done in %0.3fs" % (time() - t0))

print(classification_report(y_test, y_pred, target_names=target_names))
print(confusion_matrix(y_test, y_pred, labels=range(nb_class)))


def plot_gallery(images, titles, h, w, n_row=3, n_col=4):
    """Helper function to plot a gallery of portraits"""
    plt.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    plt.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        plt.subplot(n_row, n_col, i + 1)
        plt.imshow(images[i].reshape((h, w)), cmap=plt.cm.gray)
        plt.title(titles[i], size=12)
        plt.xticks(())
        plt.yticks(())


def title(y_pred, y_test, target_names, i):
    pred_name = target_names[y_pred[i]].rsplit(' ', 1)[-1]
    true_name = target_names[y_test[i]].rsplit(' ', 1)[-1]
    return 'predicted: %s\ntrue:      %s' % (pred_name, true_name)

prediction_titles = [title(y_pred, y_test, target_names, i)
                     for i in range(y_pred.shape[0])]

plot_gallery(X_test, prediction_titles, h, w)

plt.show()